import React, { Component } from 'react';
//import TodoList from './TodoList';
class TodoItem extends Component {
  constructor() {
    super();
    this.state = {
      dbtext: 'пусто'
    };
  }

  getText = event => {
    this.setState({ dbtext: event.target.value });
  };

  _handleKeyDown = e => {
    if (e.key === 'Enter') {
      this.props.ApplyUserImput(this.props.todo.id, this.state.dbtext);
      this.props.createInput(this.props.todo.id); //уберает инпут по нажатию enter
    }
  };

  render() {
    return (
      <>
        {!this.props.todo.isUpdating && (
          <div
            className='todo-item'
            onDoubleClick={() => this.props.createInput(this.props.todo.id)}
          >
            <input
              onClick={() => this.props.handleChange(this.props.todo.id)}
              onChange={() => {}}
              name='completed'
              type='checkbox'
              checked={this.props.todo.completed}
            />
            <p className={this.props.todo.completed ? 'completed' : ''}>
              {this.props.todo.text}
            </p>
          </div>
        )}

        {this.props.todo.isUpdating && (
          <div
            className='todo-item'
            onDoubleClick={() => this.props.createInput(this.props.todo.id)}
          >
            <input
              name='completed'
              type='checkbox'
              checked={this.props.todo.completed}
            />
            <p className={this.props.todo.completed ? 'completed' : ''}>
              {
                <input
                  onChange={this.getText}
                  onKeyDown={this._handleKeyDown}
                  name='newtextbox'
                  type='text'
                  checked={this.props.todo.completed}
                />
              }
            </p>
          </div>
        )}
      </>
    );
  }
}

export default TodoItem;
