import React, { Component } from 'react';
import todosData from './todosData.js';
import TodoItem from './TodoItem';

class TodoList extends Component {
  constructor() {
    super();
    this.state = {
      value: 'Ты точно собирался что-то делать?',
      valueOfCompletedTodoes: 0,
      valueOfSwitch: 'showall',
      newtext: '',
      todos: [
        {
          id: 1,
          text: 'Стащить код из гугла',
          completed: true,
          isUpdating: false
        },
        {
          id: 2,
          text: 'Разобраться, почему не подключается CSS',
          completed: true,
          isUpdating: false
        },
        {
          id: 3,
          text: 'Показать проект Лине',
          completed: true,
          isUpdating: false
        },
        {
          id: 4,
          text: 'Залить его на битбакет',
          completed: false,
          isUpdating: false
        },
        {
          id: 5,
          text: 'Сделать всё по нормальному',
          completed: false,
          isUpdating: false
        }
      ]
    };
  }

  ApplyUserImput = (id, value) => {
    this.setState(prev => {
      const updatedTodosList = prev.todos.map(todo => {
        if (todo.id === id) {
          todo.text = value;
        }
        return todo;
      });
      return {
        todos: updatedTodosList
      };
    });
  };

  handleChange = id => {
    this.setState(prev => {
      const updatedTodosList = prev.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      });
      return {
        todos: updatedTodosList
      };
    });
  };

  //не доделанная функция для даблклика по тудушке
  createInput = id => {
    this.setState(prev => {
      const updatedTodosList = prev.todos.map(todo => {
        if (todo.id === id) {
          todo.isUpdating = !todo.isUpdating;
        }
        return todo;
      });
      return {
        todos: updatedTodosList
      };
    });
  };

  getText = event => {
    this.setState({ value: event.target.value });
  };

  filtration = () => {
    //event.preventDefault()
    const { todos } = this.state;
    const result = todos.filter(todo => !todo.completed);
    this.deleteAll();
    this.setState(({ todos }) => ({
      todos: todos.concat(result) //добовляем объект в state
    }));
  };

  deleteAll = event => {
    //event.preventDefault()
    const { todos } = this.state;
    this.setState(({ todos }) => ({
      todos: [] //добовляем объект в state
    }));
  };

  counterOfCompletedTodoes = () => {
    const { todos } = this.state;
    let counter = 0;
    for (let todo of todos) {
      if (todo.completed) {
        counter++;
      }
    }
    return counter;
  };

  handleKeyDown = e => {
    if (e.key === 'Enter') {
      const { todos } = this.state;
      // const todos = this.state.todos; //аналог верхней записи

      let object = {
        id: todos.length + 1,
        text: this.state.value,
        completed: false
      };
      this.setState(({ todos }) => ({
        todos: todos.concat(object) //добовляем объект в state
      }));
    }
  };

  render() {
    const { todos, switches } = this.state;

    return (
      <div className='todo-field'>
        <div className='todo-list'>
          <label>
            <input
              type='text'
              onChange={this.getText}
              onKeyDown={this.handleKeyDown}
              name='name'
            />
          </label>

          {todos.map(todo => {
            switch (this.state.valueOfSwitch) {
              case 'showall':
                {
                  if (true) {
                    return (
                      <TodoItem
                        todo={todo}
                        key={todo.id}
                        handleChange={this.handleChange}
                        createInput={this.createInput}
                        ApplyUserImput={this.ApplyUserImput}
                      />
                    );
                  }
                }
                break;
              case 'showcompleted':
                {
                  if (todo.completed) {
                    return (
                      <TodoItem
                        todo={todo}
                        key={todo.id}
                        handleChange={this.handleChange}
                        createInput={this.createInput}
                        ApplyUserImput={this.ApplyUserImput}
                      />
                    );
                  }
                }
                break;
              case 'showcurrent':
                {
                  if (todo.completed == false) {
                    return (
                      <TodoItem
                        todo={todo}
                        key={todo.id}
                        handleChange={this.handleChange}
                        createInput={this.createInput}
                        ApplyUserImput={this.ApplyUserImput}
                      />
                    );
                  }
                }
                break;
            }
          })}
        </div>

        <div className='delete-done-todo'>
          <button onClick={this.filtration} type='button'>
            Delete Completed
          </button>
          <button onClick={this.deleteAll} type='button'>
            Delete All
          </button>
        </div>

        <div className='show-options'>
          <button
            onClick={() => this.setState({ valueOfSwitch: 'showall' })}
            type='button'
          >
            Show All
          </button>
          <button
            onClick={() => this.setState({ valueOfSwitch: 'showcompleted' })}
            type='button'
          >
            Show Completed
          </button>
          <button
            onClick={() => this.setState({ valueOfSwitch: 'showcurrent' })}
            type='button'
          >
            Show Сurrent
          </button>
        </div>

        <div>
          <p className='staticsOfTodoes'>
            All of Todoes: {+todos.length} Completed:{' '}
            {this.counterOfCompletedTodoes()} Сurrent:{' '}
            {+todos.length - +this.counterOfCompletedTodoes()}
          </p>
        </div>
      </div>
    );
  }
}

export default TodoList;
